# README

## I - systemctl
`systemctl` est une commande utilisée pour interagir avec le système d'init et le gestionnaire de services `systemd` sur les systèmes Linux. Voici quelques bases de `systemctl` avec des exemples pratiques d'utilisation :

1. **Démarrer un service :**
   ```bash
   sudo systemctl start nom_du_service
   ```

2. **Arrêter un service :**
   ```bash
   sudo systemctl stop nom_du_service
   ```

3. **Redémarrer un service :**
   ```bash
   sudo systemctl restart nom_du_service
   ```

4. **Activer un service au démarrage :**
   ```bash
   sudo systemctl enable nom_du_service
   ```

5. **Désactiver un service au démarrage :**
   ```bash
   sudo systemctl disable nom_du_service
   ```

6. **Vérifier l'état d'un service :**
   ```bash
   systemctl status nom_du_service
   ```

7. **Afficher tous les services avec leur état :**
   ```bash
   systemctl list-units --type=service
   ```

8. **Afficher tous les services activés/désactivés :**
   ```bash
   systemctl list-unit-files --type=service
   ```

9. **Vérifier si un service est activé/désactivé :**
   ```bash
   systemctl is-enabled nom_du_service
   ```

10. **Afficher les journaux liés à un service :**
    ```bash
    journalctl -u nom_du_service
    ```

11. **Afficher les dépendances d'un service :**
    ```bash
    systemctl list-dependencies nom_du_service
    ```

12. **Recharger la configuration d'un service sans le redémarrer :**
    ```bash
    sudo systemctl reload nom_du_service
    ```

13. **Afficher la liste des cibles (targets) disponibles :**
    ```bash
    systemctl list-units --type=target
    ```

14. **Changer la cible (target) du système (par exemple, passer de multi-user à graphical) :**
    ```bash
    sudo systemctl isolate graphical.target
    ```

15. **Afficher les informations détaillées d'un service :**
    ```bash
    systemctl show nom_du_service
    ```

Ces exemples vous donnent une idée des fonctionnalités de base de `systemctl`. N'hésitez pas à consulter la documentation (`man systemctl` ou `systemctl --help`) pour plus d'options et d'informations détaillées.

## II - journalctl

`journalctl` est une commande utilisée pour interagir avec le système de journalisation de systemd sur les systèmes Linux. Il permet de consulter et de gérer les messages du journal système. Voici quelques bases de `journalctl` avec des exemples pratiques d'utilisation :

1. **Afficher les messages du journal système :**
   ```bash
   journalctl
   ```

2. **Afficher les messages en temps réel :**
   ```bash
   journalctl -f
   ```

3. **Afficher les messages d'un service spécifique :**
   ```bash
   journalctl -u nom_du_service
   ```

4. **Afficher les messages depuis un certain temps :**
   ```bash
   journalctl --since "yyyy-mm-dd HH:MM:SS"
   ```

5. **Afficher les messages jusqu'à un certain moment :**
   ```bash
   journalctl --until "yyyy-mm-dd HH:MM:SS"
   ```

6. **Afficher un nombre spécifique de lignes :**
   ```bash
   journalctl -n nombre_de_lignes
   ```

7. **Afficher les messages avec un format court :**
   ```bash
   journalctl -o short
   ```

8. **Afficher les messages du noyau (kernel) :**
   ```bash
   journalctl -k
   ```

9. **Afficher les messages liés à une unité de journalisation spécifique :**
   ```bash
   journalctl _SYSTEMD_UNIT=nom_de_l_unite
   ```

10. **Afficher les messages de l'utilisateur courant :**
    ```bash
    journalctl --user
    ```

11. **Afficher les messages du journal à partir d'un fichier journal spécifique :**
    ```bash
    journalctl --file=/var/log/journal/xxxxxxxxxxxxx/journal.log
    ```

12. **Afficher les messages avec des informations détaillées (mode verbeux) :**
    ```bash
    journalctl -xe
    ```

Ces exemples fournissent une variété d'options pour filtrer et afficher les messages du journal selon vos besoins. N'hésitez pas à explorer davantage les options de la commande `journalctl` en consultant sa page de manuel (`man journalctl`).